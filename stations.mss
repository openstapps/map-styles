@station-color: #1C62BC;
@station-low-zoom-color: #7c7c7c;
@station-text: darken(saturate(@station-color, 15%), 10%);

.stations {
  [railway = 'subway_entrance'][zoom >= 18] {
    marker-file: url('symbols/entrance.10.svg');
    marker-placement: interior;
    marker-fill: @transportation-icon;
    marker-clip: false;
    [zoom >= 19] {
      text-name: [ref];
      text-face-name: @book-fonts;
      text-size: 10;
      text-fill: @transportation-icon;
      text-dy: 10;
      text-halo-radius: @standard-halo-radius * 1.5;
      text-halo-fill: @standard-halo-fill;
      text-wrap-width: 0;
      text-placement: interior;
    }
  }

  [subway != 'yes'] {
    [railway = 'station'][zoom >= 11] {
      marker-file: url('symbols/square.svg');
      marker-placement: interior;
      marker-fill: @station-color;
      marker-width: 1;
      marker-clip: false;
      [zoom >= 12] {
        marker-fill: @station-low-zoom-color;
        marker-width: 3;
      }
      [zoom >= 14] {
        marker-file: url('symbols/train_station.svg');
        marker-fill: @station-color;
        marker-width: 10;
        text-name: "[name]";
        text-face-name: @book-fonts;
        text-size: 10;
        text-fill: @station-text;
        text-dy: 9;
        text-halo-radius: @standard-halo-radius * 1.5;
        text-halo-fill: @standard-halo-fill;
        text-wrap-width: 30; // 3 em
        text-line-spacing: -1.5; // -0.15 em
        text-placement: interior;
      }
      [zoom >= 15] {
        marker-width: 12;
        text-size: 11;
        text-wrap-width: 33; // 3 em
        text-line-spacing: -1.65; // -0.15 em
        text-dy: 10;
      }
    }

    [railway = 'halt'] {
      [railway = 'station'][zoom >= 12] {
        marker-file: url('symbols/square.svg');
        marker-placement: interior;
        marker-fill: @station-low-zoom-color;
        marker-width: 2;
        marker-clip: false;
      }
      [zoom >= 14] {
        marker-file: url('symbols/train_station.svg');
        marker-placement: interior;
        marker-fill: @station-color;
        marker-width: 8;
        marker-clip: false;
      }
      [zoom >= 15] {
        marker-width: 10;
        text-name: "[name]";
        text-face-name: @book-fonts;
        text-size: @standard-font-size;
        text-fill: @station-text;
        text-dy: 10;
        text-halo-radius: @standard-halo-radius * 1.5;
        text-halo-fill: @standard-halo-fill;
        text-wrap-width: @standard-wrap-width;
        text-line-spacing: @standard-line-spacing-size;
        text-placement: interior;
      }
    }

    [aerialway = 'station']::aerialway {
      [zoom >= 13] {
        marker-file: url('symbols/square.svg');
        marker-placement: interior;
        marker-fill: @station-low-zoom-color;
        marker-width: 1;
        marker-clip: false;
      }
      [zoom >= 14] {
        marker-width: 4;
        text-name: "[name]";
        text-face-name: @book-fonts;
        text-size: @standard-font-size;
        text-fill: @station-text;
        text-dy: 10;
        text-halo-radius: @standard-halo-radius * 1.5;
        text-halo-fill: @standard-halo-fill;
        text-wrap-width: @standard-wrap-width;
        text-line-spacing: @standard-line-spacing-size;
        text-placement: interior;
      }
    }

    [railway = 'tram_stop'] {
      [zoom >= 15] {
        marker-file: url('symbols/square.svg');
        marker-width: 4;
        marker-placement: interior;
        marker-fill: @transportation-icon;
        marker-clip: false;
      }
      [zoom >= 16] {
        marker-file: url('symbols/tram_stop.svg');
        marker-width: 8;
        text-name: "[name]";
        text-face-name: @book-fonts;
        text-size: @standard-font-size;
        text-fill: @station-text;
        text-dy: 10;
        text-halo-radius: @standard-halo-radius * 1.5;
        text-halo-fill: @standard-halo-fill;
        text-wrap-width: @standard-wrap-width;
        text-line-spacing: @standard-line-spacing-size;
        text-placement: interior;
      }
    }
  }
  [subway = 'yes'] {
    [zoom >= 14] {
      marker-file: url('symbols/subway_german.svg');
      marker-placement: interior;
      marker-width: 8;
      marker-clip: false;
    }
    [zoom >= 16] {
      marker-file: url('symbols/subway_german.svg');
      marker-placement: interior;
      marker-width: 14;
      marker-clip: false;
      text-name: "[name]";
      text-face-name: @book-fonts;
      text-size: @standard-font-size;
      text-fill: @station-text;
      text-dy: 10;
      text-halo-radius: @standard-halo-radius * 1.5;
      text-halo-fill: @standard-halo-fill;
      text-wrap-width: @standard-wrap-width;
      text-line-spacing: @standard-line-spacing-size;
      text-placement: interior;
    }
  }
}
