@building-fill: #f2f2f2; //Lch(84, 5, 70)
@building-line: darken(@building-fill, 15%);
@building-low-zoom: lighten(@building-fill, 10%);

@building-major-fill: darken(@building-fill, 5%);
@building-major-line: darken(@building-major-fill, 25%);


#buildings {
  [zoom >= 15] {
    line-color: @building-line;
    polygon-fill: @building-fill;
    line-width: .75;
    line-clip: false;
  }
}

#buildings-major {
  [zoom >= 13] {
    [aeroway = 'terminal'],
    [amenity = 'place_of_worship'],
    [building = 'train_station'],
    [aerialway = 'station'],
    [public_transport = 'station'] {
      polygon-fill: @building-major-fill;
      polygon-clip: false;
      [zoom >= 15] {
        line-width: .75;
        line-clip: false;
        line-color: @building-major-line;
      }
    }
  }
}

#bridge {
  [zoom >= 12] {
    polygon-fill: #B8B8B8;
  }
}
