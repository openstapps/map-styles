# Documentation for StApps Map Styles

This document provides a quick introduction into the further development (styling) of the OpenStreetMap (its tiles) used in the StApps project.
The use of **docker** makes it possible to start **kosmtik** (JavaScript tool for map editing) in only a few steps, and directly after start modifying the design of the map.

## Quick Start
- clone this repository
- enter its (local) directory
- download a .pbf file (OSM data) of the required region (e.g. from http://download.geofabrik.de/)
or visit https://www.openstreetmap.org/, click "Export" button, download (export) the .osm file and with the help of tool **osmconvert** (https://wiki.openstreetmap.org/wiki/Osmconvert) generate a .pbf file:
 - `osmconvert map.osm --out-pbf>data.osm.pbf`
- the .pbf file needs to be in the root directory with the name **data.osm.pbf** (if needed its name needs to be changed into **data.osm.pbf**)
- `docker-compose up import` - import data from the file
- `docker-compose up kosmtik` - to start **kosmtik** (a.k.a. Review-Tool) and then wait until the text `[Core] Map ready` is shown
- http://0.0.0.0:6789 - access this address in the Web browser
- **Ctrl+C** - stops a **kosmtik** docker container
- `docker-compose stop db` - stops the database container

## Kosmtik
![kosmtik](osm-data-layer-off.png)

- **Settings -> OSM data layer** - if this option is turned on, it is possible to see information about a selected object, like in the following example:
![kosmtik](osm-data-layer-on.png)
This information can be very useful, in order to find related style definitions of the selected elements in MSS files.

- **Inspect -> Active** - similar option but not so useful (like OSM data layer) because of its bad performance.

- **Reference** - documentation about CartoCSS or simply said: shows the syntax for writing MSS definitions.

## CartoCSS

...is a language for styling of the OpenStreetMap (similar to CSS). More information:

[CartoCSS .mss stylesheets](https://ircama.github.io/osm-carto-tutorials/editing-guidelines/#cartocss-mss-stylesheets)

## How to make changes?

There are detailed instructions here:
[Editing Guidelines for OpenStreetMap Carto](https://ircama.github.io/osm-carto-tutorials/editing-guidelines/)

In short:

- **MML-Datei** (e.g. project.mml) contains:
 - project settings (zoom levels, a list of MSS (stylesheet) files, database settings etc.)
 - Definitions of all layers (with data) of the map, which are consisted of SQL queries (which are sent to the PostgreSQL database, in order to fetch data for these layers)
- **MSS-Dateien**
 - used for defining the appearance of the data
 - note: in "symbols" directory there are icons which are used for representing data on the map
 
## What is different in comparison with [OpenStreetMap Carto](https://github.com/gravitystorm/openstreetmap-carto) repository (except the style changes)?

- a new (adjusted) MML file (**project_mod.mml**) is used (instead of  **project.mml**)
- colors of the roads/streets are not generated (**road-colors-generate.mss** is not used); instead: colors are adjusted or entered manually into the **road-colors.mss** file
